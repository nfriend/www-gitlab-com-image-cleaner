# www-gitlab-com image cleaner

A small utility script that parses
[`team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team.yml)
in [`www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com/) and deletes
old images in `source/images/team/`.

## Instructions

1. Clone https://gitlab.com/gitlab-com/www-gitlab-com/
1. Clone this repo
1. In this repo, run `yarn`
1. In this repo, run `yarn run clean --repo-path <path/to/www-gitlab-com-repo>`,
   where `<path/to/www-gitlab-com-repo>` points to the location where you cloned
   https://gitlab.com/gitlab-com/www-gitlab-com/ in step <span>#</span>1.
1. Navigate to your https://gitlab.com/gitlab-com/www-gitlab-com/ and commit the
   deletions
